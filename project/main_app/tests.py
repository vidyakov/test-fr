import pytest
from django.urls import reverse
from mimesis import Person, Text
from mimesis.enums import Locale


@pytest.mark.django_db
def test_create_client(client, client_data):
    resp = client.post(reverse('main_app:client-list'), data=client_data)
    assert resp.status_code == 201


@pytest.mark.django_db
def test_update_client(client, client_fixture):
    resp = client.patch(
        reverse('main_app:client-detail', args=[client_fixture.id]),
        content_type='application/json',
        data={'phone': Person(Locale.RU).telephone()}
    )
    assert resp.status_code == 200


@pytest.mark.django_db
def test_delete_client(client, client_fixture):
    resp = client.delete(reverse('main_app:client-detail', args=[client_fixture.id]))
    assert resp.status_code == 204


@pytest.mark.django_db
def test_create_mailing(client, mailing_data):
    resp = client.post(reverse('main_app:mailing-list'), data=mailing_data)
    assert resp.status_code == 201


@pytest.mark.django_db
def test_get_general_stat(client):
    resp = client.get(reverse('main_app:mailing-general-stat'))
    assert resp.status_code == 200


@pytest.mark.django_db
def test_get_detail_stat(client, mailing):
    resp = client.get(reverse('main_app:mailing-stat', args=[mailing.id]))
    assert resp.status_code == 200


@pytest.mark.django_db
def test_update_mailing(client, mailing):
    resp = client.patch(
        reverse('main_app:mailing-detail', args=[mailing.id]),
        content_type='application/json',
        data={'text': Text(Locale.RU).text()}
    )
    assert resp.status_code == 200


@pytest.mark.django_db
def test_delete_mailing(client, mailing):
    resp = client.delete(reverse('main_app:mailing-detail', args=[mailing.id]))
    assert resp.status_code == 204
