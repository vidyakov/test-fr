from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

from .actors import send_message
from .models import Mailing, Client, Message


@receiver(post_save, sender=Mailing)
def start_mailing(**kwargs):
    mailing = kwargs['instance']
    if mailing.start < now() < mailing.end:
        for client in Client.objects.filter(Q(tag=mailing.tag) | Q(operator=mailing.operator)):
            message = Message.objects.create(mailing=mailing, client=client)
            send_message.send(message.id)
