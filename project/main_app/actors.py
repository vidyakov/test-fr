import http

import dramatiq
import requests
from django.conf import settings
from django.utils.timezone import now
from loguru import logger

from .models import Message

session = requests.Session()
session.headers['authorization'] = f'Bearer {settings.FABRIQUE_TOKEN}'


class SendError(Exception):
    pass


@dramatiq.actor(max_retries=3)
def send_message(message_id: int) -> None:
    message = Message.objects.get(id=message_id)
    if message.mailing.end < now():
        message.status = Message.Status.OVERDUE
        message.save()
        return

    if message.mailing.start > now():
        return

    url = (settings.FABRIQUE_URL / str(message.id)).url
    data = {
        'id': message.id,
        'phone': str(message.client.phone),
        'text': message.mailing.text
    }
    response = session.post(url, json=data)
    logger.info(f'Fabrique response status: {response.status_code}')

    if response.status_code == http.HTTPStatus.OK:
        message.status = Message.Status.SEND
        message.save()
    else:
        message.status = Message.Status.ERROR
        message.save()
        raise SendError
