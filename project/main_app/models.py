from django.db import models
from django.db.models import Count, Q
from django_extensions.db.models import TimeStampedModel
from phonenumber_field.modelfields import PhoneNumberField
from timezone_field import TimeZoneField


class Tag(models.Model):
    name = models.CharField('название', max_length=127)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'тэг'
        verbose_name_plural = 'тэги'


class Operator(models.Model):
    name = models.CharField('название', max_length=127)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'оператор'
        verbose_name_plural = 'операторы'


class Mailing(models.Model):
    start = models.DateTimeField('дата и время запуска рассылки')
    end = models.DateTimeField('дата и время окончания рассылки')
    text = models.TextField('тест сообщения')

    operator = models.ForeignKey('main_app.Operator', on_delete=models.CASCADE, null=True, blank=True)
    tag = models.ForeignKey('main_app.Tag', on_delete=models.CASCADE, null=True, blank=True)

    @classmethod
    def get_stat(cls):
        return cls.objects.aggregate(
            message_count=Count('message'),
            count=Count('id'),
            waiting=Count('id', filter=Q(message__status=Message.Status.WAITING)),
            send=Count('id', filter=Q(message__status=Message.Status.SEND)),
            error=Count('id', filter=Q(message__status=Message.Status.ERROR)),
            overdue=Count('id', filter=Q(message__status=Message.Status.OVERDUE)),
        )

    def get_detail_stat(self):
        return self.message_set.aggregate(
            message_count=Count('id'),
            waiting=Count('id', filter=Q(status=Message.Status.WAITING)),
            send=Count('id', filter=Q(status=Message.Status.SEND)),
            error=Count('id', filter=Q(status=Message.Status.ERROR)),
            overdue=Count('id', filter=Q(status=Message.Status.OVERDUE)),
        )

    def __str__(self):
        return self.text[:50]

    class Meta:
        verbose_name = 'рассылка'
        verbose_name_plural = 'рассылки'


class Client(models.Model):
    phone = PhoneNumberField('номер телефона')
    timezone = TimeZoneField('часовой пояс')

    operator = models.ForeignKey('main_app.Operator', on_delete=models.CASCADE, null=True, blank=True)
    tag = models.ForeignKey('main_app.Tag', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.phone)

    class Meta:
        verbose_name = 'клиент'
        verbose_name_plural = 'клиенты'


class Message(TimeStampedModel, models.Model):
    class Status(models.TextChoices):
        WAITING = 'waiting'
        SEND = 'send'
        ERROR = 'error'
        OVERDUE = 'overdue'

    status = models.CharField('статус', max_length=63, choices=Status.choices, default=Status.WAITING)
    mailing = models.ForeignKey('main_app.Mailing', on_delete=models.CASCADE)
    client = models.ForeignKey('main_app.Client', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.client} {self.status}'

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'
