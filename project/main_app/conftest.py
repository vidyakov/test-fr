import pytest
from mimesis import Generic
from mimesis.enums import Locale

from .models import Client, Mailing


@pytest.fixture
def client_data():
    generic = Generic(Locale.RU)
    return {
        'phone': generic.person.telephone(),
        'timezone': generic.datetime.timezone()
    }


@pytest.fixture
def client_fixture(client_data):
    return Client.objects.create(**client_data)


@pytest.fixture
def mailing_data(settings):
    generic = Generic(Locale.RU)
    return {
        'start': generic.datetime.datetime(timezone=settings.TIME_ZONE),
        'end': generic.datetime.datetime(timezone=settings.TIME_ZONE),
        'text': generic.text.text(),
    }


@pytest.fixture
def mailing(mailing_data):
    return Mailing.objects.create(**mailing_data)
